import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'paliIt';

  numbers: Array<number>;

  number: string;

  mensaje: string;

  constructor() {}

  simular() {
    if ( ! this.number ){
      return;
    } else {
      this.number = this.number + '';
    }

    if ( this.number.length < 14 || this.number.length > 16  ) {
      confirm('El numero debe ser entre 14 y 16 digitos');
      return;
    }

    this.numbers = this.findChecker();
    this.mensaje = `Se encontraron mas coincidencias del número: ${this.maxValue()}`;
    console.log(this.mensaje);
  }

  findChecker(): Array<number> {
    console.log(this.number);
    let ArrayNumber: Array<number> = [];
    this.number.split('').forEach( n => {
      if ( ArrayNumber[n] ) {
        ArrayNumber[n]++;
      }else {
        ArrayNumber[n] = 1;
      }
    });
    console.log(ArrayNumber);
    return ArrayNumber;
  }

  maxValue(): number{
    let maxIndex = 0;
    let maxValue = 0;
    this.numbers.map( (val, index) => {
       if ( val > maxValue ) {
        maxValue = val;
        maxIndex = index;
       }
    });
    return maxIndex;
  }
}
